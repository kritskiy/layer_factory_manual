/* beautify preserve:start */
//@includepath "/D/Dropbox/shared_from_win/Sync/PS/Scripts/libraries/;/Users/sergeykritskiy/Dropbox/shared_from_win/Sync/PS/Scripts/libraries/"
//@include "utilities.jsx"
//@include "polyfill.jsx"
//@include "documents.jsx"
//@include "layers.jsx"
//@include "json2.jsx"
/* beautify preserve:end */

function main()
{
    while (activeDocument.activeLayer.parent.constructor != Document) {
        activeDocument.activeLayer = activeDocument.activeLayer.parent;
    }

    var thisScriptPath = U.returnPathWithoutFile($.fileName);
    var l = L.getInfoTrgt();

    if (l.type != 7)
    {
        alert('not a group');
        return;
    }

    var clone = D.clone(),
        myName = getName(l.index),
        groupName = myName.split('/'),
        //savePath = "/Users/sergeykritskiy/Dropbox (Personal)/shared_from_win/Sync/PS/git/bakery_manual/docs/source/";
        savePath = thisScriptPath + "/docs/source";

    if (groupName.length == 1)
    {
        groupName.unshift('');
    };

    L.mergeDown();

    l = L.getInfoTrgt();

    D.rectSelection({
        left: 0,
        right: parseInt(activeDocument.width.as("px")),
        top: l.top,
        bottom: l.bottom,
    })

    D.cropToSelection();

    D.flatten();

    D.savePNGQuant(
    {
        name: groupName[1],
        path: savePath + '/' + groupName[0] + '/img'
    });

    clone.close();

    U.copyToClipboard('.. image:: img/' + groupName[1] + '.png');

    function getName(_i)
    {
        if (L.checkIfGroup(_i))
        {
            L.selectByIndex(_i)
            return L.getName(_i);
        }
        else
        {
            return getName(_i + 1);
        }
    };

}

app.activeDocument.suspendHistory("temp", "main()");