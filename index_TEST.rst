Layer Factory manual
====================

Layer Factory is a collection of scripts for Photoshop CC, CC2015-CC2018, designed to enchance and speed up different tasks related to creating and modifying layers.
 
Create new layers with blending mode of your choice, new adjustment layers, layers from selection, symmetrify layers, move selection of several layers simultaneously, convert opacity to layer mask: it’s Layer Factory! Each function module can be hidden to make sure you only have to see the tools you use on the panel.

Dock the panel in the interface or use hotkey to toggle it on and off. Most of the functions available as separate scripts that could be assigned to hotkeys. 

.. кучка гифок? на каждую функцию которые потом использовать в основных размелах
.. image:: img/start.png

--------------------------------

As for v1.0 Layer Factory includes 12 modules.
----------------------------------------------------------------

* :ref:`Add layers <addlayers>`:
* :ref:`Add ajustment layers <addlayers>`:

.. image:: general/img/layers.gif

|gif1|

|gif2|

|gif3|

--------------------------------

* :ref:`Clip multiple layers <cliplayers>`:

.. image:: clip/img/clip.gif

--------------------------------

* :ref:`Bake layers <bakelayers>`:

.. image:: bake/img/bake.gif

--------------------------------

* :ref:`Create layers from selection <layersfromselection>`:

.. image:: fromsel/img/selection.gif

--------------------------------

* :ref:`Transparency to layer masks <transptomask>`:

.. image:: opaquefy/img/tomask.gif

--------------------------------

* :ref:`Multilayer Move <multilayermove>`:

.. image:: multimove/img/mm.gif

--------------------------------

* :ref:`Temp Alpha Channel <tempchannels>`:

.. image:: tempchan/img/chan.gif

--------------------------------

* :ref:`Paths Operations <pathoperations>`:

.. image:: pathoperations/img/paths1.gif

--------------------------------

* :ref:`Quick Symmetry <symmetry>`:

.. image:: symmetry/img/sym.gif

--------------------------------

* :ref:`Smart Feather <feather>`:

.. image:: feather/img/feather.gif

--------------------------------

* :ref:`Fill The Transparency Gaps <fillthegaps>`:

.. image:: fillthegaps/img/fill.gif

--------------------------------

Contact me  at kritskiy.sergey@gmail.com |br|\
Twitter: `@ebanchiki <https://twitter.com/ebanchiki>`_ |br|\
Grab the extension on `Cubebrush <http://cbr.sh/6slma>`_ or `Gumroad <https://gum.co/brusherator>`_ 

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   ui/index
   general/index
   clip/index
   bake/index
   fromsel/index
   opaquefy/index
   multimove/index
   tempchan/index
   pathoperations/index
   symmetry/index
   feather/index
   fillthegaps/index
   hotkeys/index
   notes/index
   
.. |br| raw:: html

   <br />

.. |gif1| raw:: html
   
   <gif-player src="http://layer-factory.readthedocs.io/en/latest/_images/layers.gif" speed="1" play></gif-player>

.. |gif2| raw:: html

   <img data-gifffer="_images/layers.gif" />

.. |gif3| raw:: html

   <video controls>
     <source src="_static/layers.mp4" type="video/mp4">
     <img data-gifffer="_images/layers.gif" />
     Your browser does not support the video tag.
   </video>