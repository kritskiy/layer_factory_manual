.. |br| raw:: html

  <br />

.. |video1| raw:: html

   <video controls loop>
     <source src="../_static/09_01_paths_operations_substract.mp4" type="video/mp4">
     <img data-gifffer="../_static/09_01_paths_operations_substract.gif" />
     Your browser does not support the video tag.
   </video>

.. |video2| raw:: html

   <video controls loop>
     <source src="../_static/09_02_paths_operations_offset.mp4" type="video/mp4">
     <img data-gifffer="../_static/09_02_paths_operations_offset.gif" />
     Your browser does not support the video tag.
   </video>


.. _pathoperations:

Paths Operations
======================

This module consists of 9 functions:

.. note:: This module consists of 9 functions:

  - Paths: Combine
  - Paths: Subtract
  - Paths: Intersect
  - Paths: Exclude
  - Merge Path
  - Merge to a New Path
  - Vector Snap ON
  - Vector Snap OFF
  - Offset (Experimental)

Note that path operations work only with paths, not shape layers. If you want to use the functions with shapes, convert shape paths to ordinary paths first. 

Paths: Combine, Subtract, Intersect, Exclude
--------------------------------------------------------------

.. admonition:: Path functions
   :class: refbox

   :**Info**: These functions will change operations of selected paths to a desired operation;
   :**PS substitute**: Using Photoshop Shape Operation. Problem with it is that it uses a difficult hierarchical system;
   :**Why I use it**: Changes path operation and makes it work as expected

|video1|

To create a Shape Layer from a path you can use ``Solid Color`` from ``Adjustments`` module:

--------------------------------

Merge Path
--------------------------------

This is the same as a Photoshop Merge Shape Components

--------------------------------

Merge to a New Path
--------------------------------

This is the same as a Photoshop Merge Shape Components, but result source path will remain intact

--------------------------------

Vector Snap ON/OFF
--------------------------------

These two functions will turn Vector Snapping in Photoshop Preferences on and off

--------------------------------

Offset (Experimental)
--------------------------------

This will create a new path with a desired offset. Experimental means more like 'not polished and probably will remain like this': in the current state it doesn't remove intersecting paths, there's no preview and sharp corner points create bevels, but still this function creates a nice starting point when you need an offset. Note that this will replace the active path.

|video2|
