.. |br| raw:: html

    <br />

.. |video| raw:: html

    <iframe width="760" height="420" src="https://www.youtube.com/embed/uYz4nj1nZ2Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Layer Factory manual
====================

Layer Factory is a collection of tools for Adobe Photoshop (starting from version CC2013), designed to enhance and speed up different tasks related to creating, modifying, and working with layers.
 
Create new layers with blending modes of your choice, create new adjustment layers, layers from selection, symmetrify layers, move selection of several layers simultaneously, convert opacity to layer mask: it’s Layer Factory! Every panel module can be temporarily hidden to make sure you only see the tools you use on the panel.

Dock the panel in the interface or toggle it with a keyboard shortcut. Most of the functions are available as separate scripts that could be assigned to keyboard shortcuts as well. 

|video|

As of v1.3 Layer Factory includes 15 modules.
----------------------------------------------------------------

* :ref:`Add layers <addlayers>`: add layers with any blending mode, clip them and add layer masks in one click;
* :ref:`Add ajustment layers <addlayers>`: add adjustment layers that you want, clip them and add layer masks in one click;
* :ref:`Clip multiple layers <cliplayers>`: copy a bucnh of layers to a different bunch of layers; 
* :ref:`Bake layers <bakelayers>`: bake blended layers to normal layers;
* :ref:`Create layers from selection <layersfromselection>`: create layers from selection in a smart way;
* :ref:`Transparency to layer masks <transptomask>`: create groups and layer masks from layer transparency;
* :ref:`Multilayer Move <multilayermove>`: move/warp/liquify selection of several selected layers;
* :ref:`Temp Alpha Channel <tempchannels>`: save and restore selection quickly;
* :ref:`Paths Operations <pathoperations>`: add, subtract paths; offset paths
* :ref:`Quick Symmetry <symmetry>`: symmetrify layers across guides or paths;
* :ref:`Smart Feather <feather>`: feather selection with a preview;
* :ref:`Fill The Transparency Gaps <fillthegaps>`: fill transparent gaps with stretched pixels or layer copies;
* :ref:`Match Color <matchcolor>`: A more convenient way of using Photoshop Match Color function;
* :ref:`Arrays <arrays>`: create linear and radial arrays;
* :ref:`Labels <labels>`: use layer labels to select and hide layers;

Contact me at kritskiy.sergey@gmail.com |br|\
Twitter: `@ebanchiki <https://twitter.com/ebanchiki>`_ |br|\
`Discord <https://discord.gg/RTJydTg>`_ |br|\
Grab the extension on my `Gumroad <https://gumroad.com/kritskiy>`_ or `Cubebrush <https://cubebrush.co/kritskiy>`_

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   ui/index
   general/index
   clip/index
   bake/index
   fromsel/index
   opaquefy/index
   multimove/index
   tempchan/index
   pathoperations/index
   symmetry/index
   feather/index
   fillthegaps/index
   matchcolor/index
   arrays/index
   labels/index
   hotkeys/index
   notes/index