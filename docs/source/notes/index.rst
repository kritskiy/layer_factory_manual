Release Log
===========

31 Oct 2024: Layer Factory 1.4.4
--------------------------------

* Holding ``Ctrl`` or ``Cmd`` while using ``Symmetry`` functions will produce symmetrical copy instead of symmetryfying the layer;
* Fixed: adding a Blend If layer would sometimes create wrong Blend If values;

--------------------------------

07 Jul 2024: Layer Factory 1.4.3
--------------------------------

* New function in the Multi-module: :ref:`Rename Multiple Layers <multirename>`;
* Fixed: ``Multi-Liquify`` repeating the last transform on restart;
* Some other fixes;

--------------------------------

25 Mar 2024: Layer Factory 1.4.2
--------------------------------

* Fixed: Multi-Liquify not working properly with Clipped layers;

--------------------------------

24 Mar 2024: Layer Factory 1.4.1
--------------------------------

* Multi-Liquify: hides the active layer from the default liquify preview;
* Multi-Liquify now also works with one layer selected specifically to use this behaviour from above;
* Multi-Liquify is faster when no selection is present;
* Fixed: specfic Multi-Liquify blending modes issues;
* Fided: button Send to Brusherator not working with the latest BT;

--------------------------------

07 Feb 2024: Layer Factory 1.4.0
----------------------------------

* New 25.4 labels added (Seafoam, Indigo, Fuchsia, Magenta);
* Bake Blendif to Mask;
* Fixed: Photoshop 25.4 breaking things;
* Fixed: Add New Layer / Adjustment Layer not working when there's only a Background layer in the document;
* Fixed: Multi-Liquify not keeping opacity and blending mode of the layers;

--------------------------------

05 Dec 2022: Layer Factory 1.3.0
----------------------------------

* Added a new ``Multi-Liquify`` mode to the ``Multi-Move`` module (with an option to rasteriz layers);
* Added a new ``Separate Colors`` function to the ``Bake`` module;
* Added an options for ``Blend If`` layer to ask for a color to blend with instead of using the current color;
* Added an options for ``Layer from Selection``: to Lock Transparency and Ignore BG Layer or not and a default value to x2 Smart;
* Fixed: ``Multi-Warp`` and ``Multi-Move`` issues, some other issues, added some new issues;

--------------------------------

22 March 2021: Layer Factory 1.2.1
----------------------------------

* Option to set custom names for Labels, change Labels visibility (in the :ref:`Flyout Menu <flyoutmenu>`);
* Some options for Selection to Layer: lock transparency, ignore background layer (in the :ref:`Flyout Menu <flyoutmenu>`);
* Fixed: functions assigned to hotkeys were working slightly slower that the ones from the panel;
* Fixed: error on startup on some configs;

--------------------------------

16 March 2021: Layer Factory 1.2
--------------------------------

`Video of the update features <https://youtu.be/TAlfz4yVxjc>`_.

* New :ref:`Alternative Layout <storage>` switch;
* New Module: Arrays;
* New Module: Labels;
* New function in Bake module: :ref:`Bake to Transparency <baketotransp>`;
* New function for Symmetry module: :ref:`Repeat Last Used Symmetry <repeatsym>`;
* New function in Multilayer Move module: :ref:`Multi-Warp <multiwarp>`;
* New layer type in Add Layer module: :ref:`Blendif <blendiflayers>`;
* Bugfixes;

--------------------------------

07 Apr 2020: Layer Factory 1.1
--------------------------------

`Video with the features <https://youtu.be/pSHKq8XJh1w>`_.

* New 2 functions in the :ref:`Transparency to Masks <transptomask>` module: Add To and Subtract From Mask;
* New function in the :ref:`Bake <bakelayers>` module: Merge Unclipped layers;
* New module: :ref:`Match Color <matchcolor>`;
* ``RMB-click`` on Adjustments in the Adjustment Layers module will run this adjustment;
* Fixed: inconsistent behaviour of `Close on Click` function;
* Fixed: Smart Feather not showing blur UI when called from a hotkey;
* Fixed: unability to set hotkeys to specific keys using the Assign Shortcut command;
* Fixed: if scripts for assigning shortcuts are missing Layer Factory will show an error message;

23 Jan 2020: Layer Factory 1.0
----------------------------------

* Initial release

.. |br| raw:: html

   <br />