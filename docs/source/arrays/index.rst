.. |br| raw:: html

  <br />

.. |video1| raw:: html

   <video controls loop>
     <source src="../_static/19_lf-array-linear.mp4" type="video/mp4">
     <img data-gifffer="../_static/19_lf-array-linear.gif" />
     Your browser does not support the video tag.
   </video>

.. |video2| raw:: html

   <video controls loop>
     <source src="../_static/20_lf-array-radial.mp4" type="video/mp4">
     <img data-gifffer="../_static/20_lf-array-radial.gif" />
     Your browser does not support the video tag.
   </video>

.. _arrays:

Arrays
============================

.. note:: This module consists of 2 functions:

  - Linear Array
  - Radial Array


Linear Array
--------------------------------

.. admonition:: Linear Array
   :class: refbox

   :**Info**: This function create an array from selected layer or multiple layers with specified distance between copies or total array distance;
   :**PS substitute**: manually transform layer, if several layers selected — one by one;
   :**Why I use it**: to create grids and UI elements;

+ select the array direction;
+ set individual elements offset or total distance. If there was a selection present before running the function it can be used;
+ set the total count of objects;
+ by default the result will be made as one layer, however there's an option to make each close as a separate smart object;
+ optionally preview the result to adjust the values;

|video1|

--------------------------------

Radial Array
--------------------------------

.. admonition:: Linear Array
   :class: refbox

   :**Info**: This function create a radial array from selected layer or multiple layers using a specified pivot point and angle;
   :**PS substitute**: manually transform layer, if several layers selected — one by one;
   :**Why I use it**: to create all sorts of radial layouts;

+ select the pivot point: it could be set manually, from guides intersection, a path point or a center of a selected area;
+ set angle and total count of items;
+ set the rotation type: ``Translate Only`` won't rotate the layer;
+ by default the result will be made as one layer, however there's an option to make each close as a separate smart object;
+ optionally preview the result to adjust the values. note that preview could be blurry — this won't affect the end result;

|video2|