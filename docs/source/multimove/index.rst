.. |br| raw:: html

  <br />
  
.. |video1| raw:: html

   <video controls loop>
     <source src="../_static/07_01_multilayer_move.mp4" type="video/mp4">
     <img data-gifffer="../_static/07_01_multilayer_move.gif" />
     Your browser does not support the video tag.
   </video>
  
.. |video2| raw:: html

   <video controls loop>
     <source src="../_static/07_02_multilayer_move_copy.mp4" type="video/mp4">
     <img data-gifffer="../_static/07_02_multilayer_move_copy.gif" />
     Your browser does not support the video tag.
   </video>
  
.. |video3| raw:: html

   <video controls loop>
     <source src="../_static/07_03_multilayer_move_repeat_copy.mp4" type="video/mp4">
     <img data-gifffer="../_static/07_03_multilayer_move_repeat_copy.gif" />
     Your browser does not support the video tag.
   </video>
  
.. |video4| raw:: html

   <video controls loop>
     <source src="../_static/07_04_multilayer_move_cut_delete.mp4" type="video/mp4">
     <img data-gifffer="../_static/07_04_multilayer_move_cut_delete.gif" />
     Your browser does not support the video tag.
   </video>
  
.. |video5| raw:: html

   <video controls loop>
     <source src="../_static/18_lf-multiwarp.mp4" type="video/mp4">
     <img data-gifffer="../_static/18_lf-multiwarp.gif" />
     Your browser does not support the video tag.
   </video>
  

.. _multilayermove:

Multilayer Move
======================

.. note:: This module consists of 10 functions:

  - Move Selection
  - Copy Selection
  - Multi-Warp
  - Multi-Liquify
  - Multi-Rename
  - Repeat Move Selection
  - Repeat Copy Selection
  - Duplicate Selection
  - Cut Selection
  - Delete Selection

Every function but Multi-Warp and Multi-Liquify requires a selection and several layers selected. The functions only work on raster layers (but can work on masks of non-raster layers). Multiwarp and Multi-Liquify works with smart objects to some extent.

Move Selection
--------------------------------

.. admonition:: Move Selection
   :class: refbox

   :**Info**: This function allows to Free Transform selection of several layers;
   :**PS substitute**: None;
   :**Why I use it**: When I need to transform a selected content of several layers;

|video1|

--------------------------------

Copy Selection
--------------------------------

Same as Move Selection, but doesn't remove the selected area;

|video2|

--------------------------------

.. _multiwarp:

Multi-Warp
--------------------------------

New in LF v1.2, Multiwarp allows warping of several layers (or a selected area of several layers). Note that this function can't be assigned to a hotkey and only available from Expanded Module View. (To switch to Warping press the little ``Warp`` button in the Photoshop top Options menu after Free Transform cage is visible)

|video5|

--------------------------------

Multi-Liquify
--------------------------------

New in LF v1.3, Multi-Liquify allows running Liquify command on one or several layers.

--------------------------------

.. _multirename:

Multi-Rename
--------------------------------

New in LF 1.4.3, Multi-Rename is used to rename several selected layers following a some pattern. 

.. image:: img/m-rename.png

The window suggests several variables to use in the name like the layer current name, blending mode or a sequential number. For example:

* ``$MODE $?`` will rename the layers as their blending modes and add a sequential number: ``Normal 1``, ``Multiply 2``, ``Overlay 3``;
* ``Clouds $MODE $???`` is similar, additionally there will be a word ``Clouds`` before the blending mode, and small numbers will have two zeros added: ``Clouds Normal 001``, ``Clouds Multiply 002``, ``Clouds Overlay 003``;
* ``$NAME_$??:5.png`` will use the current layer name, add a sequential number that starts from ``5`` and will also add ``.png`` in the end — useful for using with `Generate Image Assets <https://helpx.adobe.com/photoshop/using/generate-assets-layers.html>`_, the result will look like this: ``Layer_05.png``, ``Layer Copy_06.png``, ``Layer Copy Copy Final Copy_07.png``;

--------------------------------

Repeat Move and Selection
--------------------------------

These two functions will repeat last used transformation to move or copy selected area across several layers

|video3|

--------------------------------

Duplicate, Cut, Delete Selection
--------------------------------

These three functions will duplicate, cut or delete selected area across several layers

|video4|