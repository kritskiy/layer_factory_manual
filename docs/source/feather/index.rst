.. |br| raw:: html

  <br />

.. |video1| raw:: html

   <video controls loop>
     <source src="../_static/11_01_smart_feather.mp4" type="video/mp4">
     <img data-gifffer="../_static/11_01_smart_feather.gif" />
     Your browser does not support the video tag.
   </video>

.. _feather:

Smart Feather
============================

.. note:: This module consists of 2 functions:

  - Smart Feather...
  - Repeat Smart Feather

Smart Feather...
--------------------------------

.. admonition:: Smart Feather
   :class: refbox

   :**Info**: This will show an improvised preview for Feather Selection;
   :**PS substitute**: None;
   :**Why I use it**: When I need to soften selection precisely;

|video1|

Note than because this function uses Quick Mask for a preview, this will only work with a selection outside of Quick Mask.

------------------------------------------

Repeat Smart Feather
--------------------------------

This will feather a selection with last used settings.