.. |br| raw:: html

  <br />

.. |video1| raw:: html

   <video controls loop>
     <source src="../_static/01_add_layers.mp4" type="video/mp4">
     <img data-gifffer="../_static/01_add_layers.gif" />
     Your browser does not support the video tag.
   </video>

.. |video2| raw:: html

   <video controls loop>
     <source src="../_static/02_knockout.mp4" type="video/mp4">
     <img data-gifffer="../_static/02_knockout.gif" />
     Your browser does not support the video tag.
   </video>

.. |video3| raw:: html

   <video controls loop>
     <source src="../_static/15_lf-blendif.mp4" type="video/mp4">
     <img data-gifffer="../_static/15_lf-blendif.gif" />
     Your browser does not support the video tag.
   </video>

.. _addlayers:

Add Layers and Adjustment Layers
================================

.. admonition:: Add Layers
   :class: refbox

   :**Info**: Functions in these two modules create new Layers with specifiс blending modes and mask options, and adjustment Layers;
   :**PS substitute**: Manually selecting blending modes and creating masks for new layers, adding adjustment layers from a different menu;
   :**Why I use it**: Saves clicks and time; |br|\ The layer lists consist only of the types I use;

|video1|

--------------------------------

Knockout Layers
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Layer Factory also allows to create knockout layers in one click. Knockout layers act like masks for groups: every opaque pixel punches a hole in the group making it possible to have multiple editable masks:

|video2|

--------------------------------

.. _blendiflayers:

BlendIf Layers
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

New in LF v1.2, this layer type is great for quick masking based on specific color value. The layer uses ``Blend If`` blending based on the current or a selected color (as an option available in v1.3 in the flyout menu). ``RMB-Click`` will add ``Blend If`` blending to active layer.

|video3|

--------

Options
~~~~~~~~

Use key modifiers for more options:

* Hold ``Ctrl/Cmd`` for the new layer to become a clipping mask to the active one;
* Hold ``Alt`` to reverse ``Hide Panel On Click`` option;
* Hold ``Shift`` with a selection available to use the selection as a layer mask;
* Add Layers module: * Hold ``Shift`` without a selection to fill the new layer with a neutral color (for certain layer types: Overlay, Color Dodge, etc);
* Add Layers module: ``RMB-Click`` to change active layer to the specific blending mode;
* Add Adjustments module: Hold ``Shift`` when creating a layer to open the Properties Photoshop panel;

.. image:: img/types.png

All the key modifiers can be combined.

* ``Gradient Map`` adjustment layer will use current ``Foreground Color`` as a color between black and white;
* If several layers are selected, there will be a new layer created for each selected layer.

---------

Display only the layers you need
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Toggle specific Layer types and Adjustment Layers from ``Select List Modules Items`` flyout menu or from the bottom menu.

.. image:: ../ui/img/listmodule.png
