.. |br| raw:: html

  <br />
  
.. |video1| raw:: html

   <video controls loop>
     <source src="../_static/04_01_bake_to_normal.mp4" type="video/mp4">
     <img data-gifffer="../_static/04_01_bake_to_normal.gif" />
     Your browser does not support the video tag.
   </video>
  
.. |video2| raw:: html

   <video controls loop>
     <source src="../_static/04_02_from_clip_mask.mp4" type="video/mp4">
     <img data-gifffer="../_static/04_02_from_clip_mask.gif" />
     Your browser does not support the video tag.
   </video>
  
.. |video3| raw:: html

   <video controls loop>
     <source src="../_static/04_03_grey_to_transp.mp4" type="video/mp4">
     <img data-gifffer="../_static/04_03_grey_to_transp.gif" />
     Your brows
  
.. |video4| raw:: html

   <video controls loop>
     <source src="../_static/04_04_merge_unclipped.mp4" type="video/mp4">
     <img data-gifffer="../_static/04_04_merge_unclipped.gif" />
     Your browser does not support the video tag.
   </video>
  
.. |video5| raw:: html

   <video controls loop>
     <source src="../_static/16_lf-bake-to-tr.mp4" type="video/mp4">
     <img data-gifffer="../_static/16_lf-bake-to-tr.gif" />
     Your browser does not support the video tag.
   </video>

.. _bakelayers:

Bake Layers
======================================

.. note:: This module consists of 5 functions:

  - Bake to Normal
  - Bake to Transparency
  - Bake from Clipping Mask
  - Merge Unclipped layers
  - Grey to Transparency

Bake to Normal
-------------------------------

.. admonition:: Bake to Normal
   :class: refbox

   :**Info**: This function will convert selected layers to a Normal blending layers without any transparent pixels;
   :**PS substitute**: None;
   :**Why I use it**: To merge together layers with different blending modes or with different Blend If properties; |br|\ Minimizing a layer surface after using a large mixer brush on a merged layer;

|video1|

--------

.. _baketotransp:

Bake to Transparency
--------------------------------

New in LF 1.2, this function is similar to ``Bake to Normal``, but the resulting pixels are transparent. This function is great for separating objects from background with all their shadows.

In this example I use Bake to Transparency to bake a result of Blend If and Overlay layers together:

|video5|

--------------------------------

Bake from Clipping Mask
--------------------------------

.. admonition:: Bake from Clipping Mask
   :class: refbox

   :**Info**: This function will bake all the clipping masks and parent layer to a new layer;
   :**PS substitute**: Manually selecting, duplicating, merging, clipping;
   :**Why I use it**: To quickly merge all the clipping layers to paint with smudge or mixer brush on top;

.. note:: Options

  - The function also can be used to create a new layer from selected layers;

|video2|

--------

Merge Unclipped
--------------------------------

.. admonition:: Merge Unclipped
   :class: refbox

   :**Info**: This function will merge selected layers while ignoring clipped layers and reclip everything back;
   :**PS substitute**: Unclipping all the layers, merge parents, rearranging the clipped layers, reclipping back everything;
   :**Why I use it**: To quickly merge all parents without clipped layers; To add more pixels to a layer with clipped layers

|video4|

--------

Grey to Transparency
--------------------------------

.. admonition:: Grey to Transparency
   :class: refbox

   :**Info**: This function will create a layer with transparency based on grey values of an active document or selection;
   :**PS substitute**: Manually merging, selecting, modifying the selection, creating a layer, filling with color;
   :**Why I use it**: To quickly create line art on transparency from scanned line art;

.. note:: Options

  - Holding ``Ctrl/Cmd`` while using calling the function will prompt for Levels adjustment to manually edit a histogram;

|video3|