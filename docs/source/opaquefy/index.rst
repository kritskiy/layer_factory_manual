.. |br| raw:: html

  <br />

.. |video1| raw:: html

  <video controls loop>
    <source src="../_static/06_01_transparency_to_group.mp4" type="video/mp4">
    <img data-gifffer="../_static/06_01_transparency_to_group.gif" />
    Your browser does not support the video tag.
  </video>

.. |video2| raw:: html

  <video controls loop>
    <source src="../_static/06_02_transparency_to_mask.mp4" type="video/mp4">
    <img data-gifffer="../_static/06_02_transparency_to_mask.gif" />
    Your browser does not support the video tag.
  </video>

.. |video3| raw:: html

  <video controls loop>
    <source src="../_static/06_03_defringe.mp4" type="video/mp4">
    <img data-gifffer="../_static/06_03_defringe.gif" />
    Your browser does not support the video tag.
  </video>

.. |video4| raw:: html

  <video controls loop>
    <source src="../_static/06_04_add_to_mask.mp4" type="video/mp4">
    <img data-gifffer="../_static/06_04_add_to_mask.gif" />
    Your browser does not support the video tag.
  </video>

.. _transptomask:

Transparency to Mask
======================

.. note:: This module consists of 5 functions:

  - Transparency to Group Mask
  - Transparency to Layer Mask
  - Defringe
  - Add to Mask
  - Subtract from Mask

Transparency to Group Mask
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. admonition:: Transparency to Group Mask
   :class: refbox

   :**Info**: This function will create a group with a mask based on a layer(s) transparency with some bleeding;
   :**PS substitute**: Creating a group with layer mask based on a layer transparency, copying and merging a layer 10 times to get rid of transparent pixels;
   :**Why I use it**: New layers within the group will be limited by a group mask so all the layers will work as clipping masks. |br|\ This is useful for complex images with lots of clipping masks and layers;

In this example a texture layer ``Overlay`` works as a clipping mask for ``keys`` but it can have its own clipping makss (``Hue/Saturation`` and ``Curves`` adjustment layers that influence only the ``Overlay`` layer):

|video1|

--------------------------------

Transparency to Layer Mask
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. admonition:: Transparency to Layer Mask
   :class: refbox

   :**Info**: This function will create a layer mask based on a transparency and fill transparent pixels with the foreground color;
   :**PS substitute**: Manually creating a new layer, filling it with color, merging with target layer, creating a mask.;
   :**Why I use it**: This can be used to modify layer transparency with adjustments (Levels, Curves, etc)... |br|\ and to use Mixer Brush/Smudge Tool to modify transparency

|video2|

------------------

Defringe
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. admonition:: Defringe
   :class: refbox

   :**Info**: This function will attempt to get rid of fringing on borders of transparent layers;
   :**PS substitute**: ``Layer > Matting > Defringe`` attempts to do the same but fails miserably;
   :**Why I use it**: Useful to get rid of fringing on layers that were cut from a background;

|video3|

--------------------------------

Add to/Subtract from Mask
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. admonition:: Add to/Subtract from Mask
   :class: refbox

   :**Info**: These functions will add a selection to or subtract a selection from a mask of an active layer or its parent group;
   :**PS substitute**: Selecting a mask or a parent group mask, making sure the correct color is selected, filling it, select the layer back;
   :**Why I use it**: to quickly add to or subtract from the existing layer mask;

|video4|