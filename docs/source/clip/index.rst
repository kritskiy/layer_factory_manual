.. |br| raw:: html

  <br />

.. |video1| raw:: html

   <video controls>
     <source src="../_static/01_clip_to_parts.mp4" type="video/mp4">
     <img data-gifffer="../_static/01_clip_to_parts.gif" />
     Your browser does not support the video tag.
   </video>

.. |video2| raw:: html

   <video controls>
     <source src="../_static/02_clip_layer.mp4" type="video/mp4">
     <img data-gifffer="../_static/02_clip_layer.gif" />
     Your browser does not support the video tag.
   </video>


.. _cliplayers:

Clip Layers to Targets
=======================================

.. admonition:: Clip Layers to Targets
   :class: refbox

   :**Info**: This function will clip (or clip and merge) all selected layers to target layers;
   :**PS substitute**: Manually copying and clipping selected layers between target layers;
   :**Why I use it**: Saves clicks and time; |br|\ Works great for quickly distributing adjustments or flat layers (used for large mixer brush strokes) among target layers;

Usage
~~~~~~

#. Select the layers that will be targets for clipping. 
#. Use ``Mark Target Layers`` command;
#. Select the clipping layer(s) that you want to clip to the target layers. 
#. Use ``Clip to Targets`` to clone selected layers to their targets. Or use ``Clip to Targets & Merge`` to clip and merge the clones;

---------------

Usage scenarios
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#. I have a character cut for animation with several new adjustments applied to a whole body: adjustment layers and merged overpaint layer
#. So after using Clip to Targets & Merge, every animation layer has adjustments applied to them.

|video1|

------------------

#. I have several robowes in a group. A layer with some mixer brush strokes is clipped to it.
#. After using Clip to Targets, every hobo-robo has it’s own copy of clipped layer, and I can move them freely and edit separately.

|video2|
