Interface and Settings
======================

The panel is accessible from ``Window > Extensions > Layer Factory`` Photoshop menu. Layer Factory consists of different modules with specific functions. 

.. image:: img/use.gif

Each module can be hidden from the panel if not used (and still accessible via :ref:`Storage<storage>`). Different settings can be set via :ref:`Flyout<flyoutmenu>` or Settings menus in the panel bottom.

.. image:: img/ui.png

-----------

Using the Panel
--------------------------------

There're two types of modules on the panel:

#. List Modules (with lists of layer blending modes and adjustment layers)
#. Square Modules (that contain several functions within them)

.. image:: img/modulestypes.png

--------------------------------

List Modules
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* To change the visible items of a list module, use ``Select List Modules Items`` button in the bottom menu or in the flyout menu.

.. image:: img/listmodule.png

--------------------------------

Square Modules
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Click a Square Module to access its context menu;
* Square Modules can be expanded via Settings menu to have all the context menu items always visible;

.. image:: img/squaremodule.png

--------------------------------

.. _flyoutmenu:

Fly-out menu
------------

A number of settings and functions is can be set from Flyout menu.

+ ``Display All Hidden Modules`` will unhide all the modules;
+ ``Display Robo Helper`` will show the welcome screen;
+ ``Assign Hotkeys...`` will allow to assign hotkeys to different functions and send them as buttons to `Brusherator <https://brusherator-manual.readthedocs.io/en/latest/>`_;

* ``Show Modules Tabs on the Panel``: will toggle visibility of module names on top of the panel;
* ``Hide Panel On Click``: The panel will close after using any function. Works together with :ref:`assigning a hotkey to toggle the panel <hotkeys>`. ``Alt+Click`` on a function will reverse this option (so the panel will close if the option is off and won't close if it's on);
* ``Change Modules Options`` allows to hide and expand modules;
* ``Select List Modules Items`` allows to select the items visible in List Modules (layers and adjustments);

Update 1.2.1 also has added some modules-specific settings:

+ ``Layer From Selection...``: options for ignoring background layer and locking the transparency of the resulting layer;
+ ``Rename Labels...``: rename color labes and toggle visibility of particular labels;

Update 1.3.0 also has added some more modules-specific settings:

+ ``New Layers...``: option for the ``Blend If`` layer to use the active color or to select the layer to blend with with a color picker;
+ ``Multi-Liquify...``: option to keep or rasterize smart objects;

Here's an example of using the panel with ``Hide Panel On Click`` option turned on and calling it back using a hotkey:

|video3|

-----------

.. _storage:

Bottom menu
------------

Some of the settings can be set from the bottom menu.

* ``Hide Panel On Click``: The panel will close after using any function. Works together with :ref:`assigning a hotkey to toggle the panel <hotkeys>`. ``Alt+Click`` on a function will reverse this option (so the panel will close if the option is off and won't close if it's on);
* ``Change Modules Options`` allows to hide and expand modules;
* ``Select List Modules Items`` allows to select the items visible in List Modules (layers and adjustments);
* ``Alternative Layout`` allows to switch between two different layouts of Layer Factory modules;
* ``Storage`` provides access to the modules that are hidden from the panel;
* ``Help`` will show this manual;

.. image:: img/bottommenu.png

Here's the Storage showing the modules that were hidden from the panel:

.. image:: img/bottom.png

--------------------------------

Everything from this page and more as a video:

|video|

.. |br| raw:: html

   <br />

.. |video3| raw:: html

   <video controls loop>
     <source src="../_static/02_usage_with_hide.mp4" type="video/mp4">
     <img data-gifffer="../_static/02_usage_with_hide.gif" />
     Your browser does not support the video tag.
   </video>

.. |video| raw:: html

    <iframe width="760" height="420" src="https://www.youtube.com/embed/p3kuwLsmZkM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>