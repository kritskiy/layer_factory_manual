.. |br| raw:: html

  <br />

.. |video1| raw:: html

   <video controls loop>
     <source src="../_static/12_01_fill_the_transparency_gaps_stretch.mp4" type="video/mp4">
     <img data-gifffer="../_static/12_01_fill_the_transparency_gaps_stretch.gif" />
     Your browser does not support the video tag.
   </video>

.. |video2| raw:: html

   <video controls loop>
     <source src="../_static/12_02_fill_the_transparency_gaps_clone.mp4" type="video/mp4">
     <img data-gifffer="../_static/12_02_fill_the_transparency_gaps_clone.gif" />
     Your browser does not support the video tag.
   </video>

.. _fillthegaps:

Fill The Transparency Gaps
============================

.. note:: This module consists of 4 functions:

  - Fill Horizontally
  - Fill Vertically
  - Horizontally: force stretch
  - Verticallyy: force stretch

Fill Horizontally, Vertically
--------------------------------

.. admonition:: Fill The Transparency Gaps
   :class: refbox

   :**Info**: These functions will fill the transparent gaps with selected row of pixels;
   :**PS substitute**: Manually stretching or copy-pasting and moving pixels to fill the gaps;
   :**Why I use it**: To fill selection with repeating details;

If your selection contains a pixel row/column, pixels will be stretched; otherwise pixels will be cloned inside a selection rectangle. To force stretch a pixel row/column, use a ``force stretch`` function.

|video1|

------------------------------------------

Horizontally, Vertically: force stretch
----------------------------------------------------------------

These are similar to ``Fill Horizontally``, ``Fill Vertically``, but use only a stretched one pixel row or column to fill a selection rectangle.

|video2|

