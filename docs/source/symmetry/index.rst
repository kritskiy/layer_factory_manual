.. |br| raw:: html

  <br />

.. |video1| raw:: html

   <video controls loop>
     <source src="../_static/10_01_symmetrify_layer.mp4" type="video/mp4">
     <img data-gifffer="../_static/10_01_symmetrify_layer.gif" />
     Your browser does not support the video tag.
   </video>

.. |video2| raw:: html

   <video controls loop>
     <source src="../_static/10_02_symmetrify_layer_path_sym.mp4" type="video/mp4">
     <img data-gifffer="../_static/10_02_symmetrify_layer_path_sym.gif" />
     Your browser does not support the video tag.
   </video>

.. _symmetry:

Symmetrify Layer
======================

.. note:: This module consists of 6 functions:

  - Quick Symmetry
  - Repeat Last Used Symmetry
  - Sym Left to Right
  - Sym Right to Left
  - Sym Top to Bottom
  - Sym Bottom to Right

.. admonition:: Quick Symmetry
   :class: refbox

   :**Info**: This function will make a symmetrical copy of selected layers or paths using a horizontal or vertical Guide line in a document or a path as a symmetry plane;
   :**PS substitute**: None; |br|\ Symmetrical painting in Photoshop CC2018 helps with painting but unable to symmetrify the existing layers;
   :**Why I use it**: To make a symmetrical copy of existing layers;

Quick Symmetry
--------------------------------------------------------------

Note that this function only works with ordinary layers (not Type, Smart Objects, Shape Layers, etc) or paths. For paths, the function will copy the selected paths without replacing any existing paths across the symmetry plane.

If you're not familiar with Photoshop Guides, `try this article <https://helpx.adobe.com/photoshop/using/grid-guides.html>`_. Default direction for a symmetrical copy is left to right for horizontal symmetry and top to bottom for a vertical.

|video1|

.. _repeatsym:

Repeat Last Used Symmetry
--------------------------------

New in LF 1.2, this function allows repeating the previous sym action. Say I'm working on a piece that has two or more sym axes, or I have several assets in different documents that use the same axis. I can use the sym command I want once and then just use repeat to do the same sym type repeatedly.

--------------------------------

Using Paths
--------------------------------

If a path that consists only of 2 points is selected, it'll be used as a symmetry plane:

|video2|

--------------------------------

Sym Left to Right, Right to Left, Top to Bottom, Bottom to Right
------------------------------------------------------------------------------------------------

Similar to Quick Symmetry, these functions will make a symmetrical copy of selected layers using a horizontal or vertical Guide line in a document as a symmetry plane with a specific direction. Additionally, new in 1.4.4, holding ``Ctrl`` on Windows or ``Cmd`` on Mac will create a symmetrical copy of the layer instead of symmetryfying it.



