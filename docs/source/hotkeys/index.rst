.. |br| raw:: html

   <br />

.. |video1| raw:: html

   <video controls loop>
     <source src="../_static/13_01_assigning_hotkeys_lf.mp4" type="video/mp4">
     <img data-gifffer="../_static/13_01_assigning_hotkeys_lf.gif" />
     Your browser does not support the video tag.
   </video>
.. |video2| raw:: html

   <video controls loop>
     <source src="../_static/13_02_assigning_hotkeys_ps.mp4" type="video/mp4">
     <img data-gifffer="../_static/13_02_assigning_hotkeys_ps.gif" />
     Your browser does not support the video tag.
   </video>

Assigning Hotkeys
====================

.. _hotkeys:

Photoshop CC2015 and newer
--------------------------------

To assign most of the functions to hotkeys you can use ``Assign Hotkeys...`` option in flyout menu. Simply choose a category, select a function and use an ``Assign...`` button to assign a hotkey.

|video1|

Also it's possible to send any script directly to `Brusherator <http://gum.co/brusherator>`_ from this window using the ``Send to Brusherator`` button.

--------------------------------

Photoshop CC and CC2014
---------------------------------

Navigate to Photoshop menu ``Edit > Keyboard Shortcuts`` and then find the scripts that start with ``LF`` prefix. Assign hotkeys as usual.  

|video2|