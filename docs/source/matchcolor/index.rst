.. |br| raw:: html

  <br />

.. |video1| raw:: html

   <video controls loop>
     <source src="../_static/14_01_match_color.mp4" type="video/mp4">
     <img data-gifffer="../_static/14_01_match_color.gif" />
     Your browser does not support the video tag.
   </video>

.. _matchcolor:

Match Color
============================

.. admonition:: Match Color
   :class: refbox

   :**Info**: This function will start a Match Color adjustment with specific source and target already set;
   :**PS substitute**: preparing the source for Match Color before using the function, setting the source image and layer among the 10000 layers you have;
   :**Why I use it**: To quickly use Photoshop match color functionality without spending hours on setting everything up;

+ Simply running this function will use a merged copy of all layers (but the active one) as a source for Match Color;
+ Having a selection in the document will use a merged copy of the selected area as a source;
+ Having two layers selected will use the top layer as a target and the botom layer as a source;
+ Holding the ``Ctrl``-key will apply the adjustment to a clipped copy of a target layer;

In this video I'm matching a new part of Roboguy using a selection. I was holding the ``Ctrl``-key so Match Color was used on a copy of the layer:

|video1|