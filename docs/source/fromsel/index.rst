.. |br| raw:: html

  <br />

.. |video1| raw:: html

  <video controls loop>
    <source src="../_static/05_01_selection_to_layer.mp4" type="video/mp4">
    <img data-gifffer="../_static/05_01_selection_to_layer.gif" />
    Your browser does not support the video tag.
  </video>

.. |video2| raw:: html

  <video controls loop>
    <source src="../_static/05_02_selection_to_layer_bad.mp4" type="video/mp4">
    <img data-gifffer="../_static/05_02_selection_to_layer_bad.gif" />
    Your browser does not support the video tag.
  </video>

.. |video3| raw:: html

  <video controls loop>
    <source src="../_static/05_03_selection_to_smart.mp4" type="video/mp4">
    <img data-gifffer="../_static/05_03_selection_to_smart.gif" />
    Your browser does not support the video tag.
  </video>

.. _layersfromselection:

Selection to Layer
======================

.. note:: This module consists of 2 functions:

  - Selection to Layer;
  - Selection to x2 Smart Object;

Selection to Layer
--------------------------------

.. admonition:: Selection to Layer
   :class: refbox

   :**Info**: This function will create a new layer from selection with locked transparency from the layers beneath. |br|\ Layers above the active layer will be ignored: this is important when layers above aren't 100% opacity or aren't Normal blendmode layers;
   :**PS substitute**: Manually turning off all the layers above an active one, merging visible layers, maskign resulting layer with selection, turning on all the layers that were turned off;
   :**Why I use it**: To get a layer from selection without interference of above layers; |br|\ Resulting layer has locked transparency so I can start painting imidiately;

|video1|

The same document with a layer made with ``Merge Visible``: a blue Soft Light layer was also influencing the merge, so the result is more blue than it should be:

|video2|

New in 1.2.1, it's also possible to set some options for the function in the ``Layer From Selection`` option from the :ref:`Flyout Menu <flyoutmenu>` — to lock/not lock transparency and to ignore the background layer.

---------

Selection to x2 Smart Object
--------------------------------

.. admonition:: Selection to x2 Smart Object
   :class: refbox

   :**Info**: Similar to ``Selection to Layer`` but resulting layer will be a smart object with double the resolution;
   :**Why I use it**: To add some details to specific areas;

|video3|