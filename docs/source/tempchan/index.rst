.. |br| raw:: html

  <br />

.. |video1| raw:: html

   <video controls loop>
     <source src="../_static/08_01_temporary_alpha_channel.mp4" type="video/mp4">
     <img data-gifffer="../_static/08_01_temporary_alpha_channel.gif" />
     Your browser does not support the video tag.
   </video>

.. _tempchannels:

Temporary Alpha Channel
==========================

.. note:: This module consists of 3 functions:

  - Create Temporary Channel
  - Load Temporary Channel
  - Delete Temporary Channel

--------------------------------

Create Channel
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. admonition:: Create Temporary Channel
   :class: refbox

   :**Info**: This function will create a channel from selection;
   :**PS substitute**: Creating a channel from Channels panel;
   :**Why I use it**: In conjunction with Load and Delete this can be used to quickly save and load selections;

|video1|

By default a channel ``temp_selector`` will be created. If you hold ``Ctrl/Cmd`` while clicking on the function, you'll be prompted with a new name. 

--------------------------------

Load Channel 
--------------------------------

This function will load a ``temp_selector`` channel as a selection. If you hold ``Ctrl/Cmd`` while clicking on the function, a UI with available channels will be shown from which you can:

* Load any channel as selection
* Add to/Subtract from/Intersect with an existing selection
* Delete any user channel

By default clicking any button in the window will close it. Hold ``Alt`` to keep the window shown.

--------------------------------

Delete Channel
--------------------------------

This function will delete a ``temp_selector`` channel.