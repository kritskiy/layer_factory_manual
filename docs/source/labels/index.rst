.. |br| raw:: html

  <br />

.. |video1| raw:: html

   <video controls loop>
     <source src="../_static/17_lf-labels.mp4" type="video/mp4">
     <img data-gifffer="../_static/17_lf-labels.gif" />
     Your browser does not support the video tag.
   </video>

.. _labels:

Labels
============================

.. admonition:: Labels
   :class: refbox

   :**Info**: Assing labels, select and toggle their visibility;
   :**PS substitute**: none;
   :**Why I use it**: Layer organization;

+ Clicking on a button assigns color to selected layer(s);
+ Shift-clicking on a button selects layers with this label;
+ Cltr-clicking on a button toggles visibility the layers with this label;

New in 1.2.1, it's also possible to rename and hide labels from ``Rename Labels`` option in the :ref:`Flyout Menu <flyoutmenu>`.

|video1|